# app/controllers/api/v1/images_controller.rb
module Api
  module V1
    class ImagesController < ApplicationController
      before_action :set_image, only: [:show, :edit, :update, :destroy]
      # Prevent CSRF attacks by raising an exception.
      # For APIs, you may want to use :null_session instead.
      protect_from_forgery :except => [:create]

      # GET /api/v1/images
      # GET /api/v1/images.json
      def index
        unless params[:to_csv].blank?
          trial = params[:to_csv].to_i
          if trial.zero?
            # 0を指定されたら全部返す
            render json: {csv: Image.all.to_csv}
          else
            render json: {csv: Image.where(trial: trial).to_csv}
          end
        else
          @images = Image.all
        end
      end

      # GET /api/v1/images/1
      # GET /api/v1/images/1.json
      def show
      end

      # GET /api/v1/images/new
      def new
        @image = Image.new
      end

      # GET /api/v1/images/1/edit
      def edit
      end

      # POST /api/v1/images
      # POST /api/v1/images.json
      def create
        @image = Image.new(image_params)
        # tmp_image_params = image_params
        # image_data = base64_conversion(tmp_image_params[:remote_image_url])
        # tmp_image_params[:image] = image_data
        # tmp_image_params[:remote_image_url] = nil
        # @image = Image.new(tmp_image_params)
        # @image.save
        respond_to do |format|
          if @image.save
            format.html { redirect_to @image, notice: 'Image was successfully created.' }
            format.json { render :show, status: :created, location: @image }
          else
            format.html { render :new }
            format.json { render json: @image.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /api/v1/images/1
      # PATCH/PUT /api/v1/images/1.json
      def update
        respond_to do |format|
          if @image.update(image_params)
            format.html { redirect_to @image, notice: 'Image was successfully updated.' }
            format.json { render :show, status: :ok, location: @image }
          else
            format.html { render :edit }
            format.json { render json: @image.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /api/v1/images/1
      # DELETE /api/v1/images/1.json
      def destroy
        @image.destroy
        respond_to do |format|
          format.html { redirect_to images_url, notice: 'Image was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_image
          @image = Image.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def image_params
          response = params.require(:image).permit(:image, :map_id, :node_id, :node_name, :image_cache, :remove_image, :begin_node, :comment, :trial)
          # ノード名で登録
          trans_node_name2id(response)
        end

        def trans_node_name2id(params)
          # ノード名で登録
          unless params[:node_name].nil?
            unless Node.find_by(name: params[:node_name]).nil?
              params[:node_id] = Node.find_by(name: params[:node_name]).id
            end
            params.delete(:node_name)
          end
          params
        end
    end
  end
end
