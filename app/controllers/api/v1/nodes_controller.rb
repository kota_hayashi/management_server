# app/controllers/api/v1/maps/1/nodes_controller.rb
module Api
  module V1
    class NodesController < ApplicationController
      before_action :set_node, only: [:show, :edit, :update, :destroy, :show_next]

      # GET /maps/1/nodes
      # GET /maps/1/nodes.json
      def index
        @map = Map.find(params[:map_id])
        @nodes = @map.nodes.all
      end

      # GET /maps/1/maps/1/nodes/1
      # GET /maps/1/nodes/1.json
      def show
      end

      # GET /maps/1/nodes/new
      def new
        @node = Node.new
      end

      # GET /maps/1/nodes/1/edit
      def edit
      end

      # POST /maps/1/nodes
      # POST /maps/1/nodes.json
      def create
        @node = Node.new(node_params)

        respond_to do |format|
          if @node.save
            format.html { redirect_to @node, notice: 'Node was successfully created.' }
            format.json { render :show, status: :created, location: @node }
          else
            format.html { render :new }
            format.json { render json: @node.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /maps/1/nodes/1
      # PATCH/PUT /maps/1/nodes/1.json
      def update
        updated_time = Time.zone.now
        respond_to do |format|
          if @node.update(node_params)
            # ノードの更新があった場合、それをマップにも記録
            @node[:last_update] = updated_time
            @node.save
            @map[:last_update] = updated_time
            @map.save
            format.html { redirect_to @node, notice: 'Node was successfully updated.' }
            format.json { render :show, status: :ok, location: @node }
          else
            format.html { render :edit }
            format.json { render json: @node.errors, status: :unprocessable_entity }
          end
        end
      end

      def show_next
      end

      def show_usable
        @map = Map.find(params[:map_id])
        unless params[:modified_after].blank?
          # クエリパラメータで最終更新時間指定された場合、差分を返す
          modified_time = Time.zone.parse(params[:modified_after])
          # FIXME:intとして比較してるが精度は低い
          # logger.debug @map[:last_update].to_i

          if @map[:last_update].to_i.eql?(modified_time.to_i)
            # 更新が必要ない場合、変化がないことを伝える
            render :json => {:diff => "no", :modified_time => modified_time, :last_update => @map[:last_update]}
          else
            # 更新が必要な場合
            # 更新があったノードのチェック
            payload = {:diff => "yes", :latest_time => @map[:last_update]}
            @map.nodes.each do |node|
              if node.last_update.to_i > modified_time.to_i
                # 最終更新が修正時間より後なら
                payload[node.name] = node.usable
              end
            end

            render :json => payload
          end
        else
          # ノードの可用性のみ返す
          payload = {}
          @map.nodes.each do |node|
            payload[node.name] = node.usable
          end
          render :json => payload
        end
      end

      # DELETE /maps/1/nodes/1
      # DELETE /maps/1/nodes/1.json
      def destroy
        @node.destroy
        respond_to do |format|
          format.html { redirect_to nodes_url, notice: 'Node was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_node
          @map = Map.find(params[:map_id])
          @nodes = @map.nodes.all
          @node = @nodes.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def node_params
          params.require(:node).permit(:name, :x, :y, :z, :usable)
        end
    end
  end
end
