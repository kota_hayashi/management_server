# app/controllers/api/v1/robots_controller.rb
module Api
  module V1
    class RobotsController < ApplicationController
      before_action :set_robot, only: [:show, :edit, :update, :destroy]
      protect_from_forgery :except => [:create, :update]

      # GET /api/v1/robots
      # GET /api/v1/robots.json
      def index
        unless params[:map_name].blank?
          # クエリパラメータでマップ名を指定された場合
          map = Map.find_by(name: params[:map_name])
          unless map.nil?
            @robots = map.robots
          end
        else
          @robots = Robot.all
        end
      end

      # GET /api/v1/robots/1
      # GET /api/v1/robots/1.json
      def show
      end

      # GET /api/v1/robots/new
      def new
        @robot = Robot.new
      end

      # GET /api/v1/robots/1/edit
      def edit
      end

      # POST /api/v1/robots
      # POST /api/v1/robots.json
      def create
        @robot = Robot.new(register_robot(robot_params))

        respond_to do |format|
          if @robot.save
            format.html { redirect_to @robot, notice: 'Robot was successfully created.' }
            format.json { render :show, status: :created, location: @robot }
          else
            format.html { render :new }
            format.json { render json: @robot.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /api/v1/robots/1
      # PATCH/PUT /api/v1/robots/1.json
      def update
        respond_to do |format|
          if @robot.update(robot_params)
            format.html { redirect_to @robot, notice: 'Robot was successfully updated.' }
            format.json { render :show, status: :ok, location: @robot }
          else
            format.html { render :edit }
            format.json { render json: @robot.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /api/v1/robots/1
      # DELETE /api/v1/robots/1.json
      def destroy
        @robot.destroy
        respond_to do |format|
          format.html { redirect_to robots_url, notice: 'Robot was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      # パラメータでのjson取得のテスト
      def debug
        unless params[:map].blank?
          render json: Robot.find_by(map: params[:map])
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_robot
          @robot = Robot.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def robot_params
          response = params.require(:robot).permit(:name, :map_id, :x, :y, :z, :last_conn, :ip_add, :port, :map_name, :node)
          # マップ名で登録
          trans_map_name2id(response)
          # params.require(:robot).permit(:name, :map, :ip_add)
        end

        def trans_map_name2id(params)
          # マップ名で登録
          unless params[:map_name].nil?
            unless Map.find_by(name: params[:map_name]).nil?
              params[:map_id] = Map.find_by(name: params[:map_name]).id
            end
            params.delete(:map_name)
          end
          params
        end

        # ロボット登録に当たって名前を割り振り時間を記録する。
        def register_robot(params)
          # 時間を利用してロボットに名前を与える
          params[:name] = "robot%10.6f" % Time.now.to_f
          params[:last_conn] = Time.zone.now
          params
        end
    end
  end
end
