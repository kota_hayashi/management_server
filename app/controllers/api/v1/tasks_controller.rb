# app/controllers/api/v1/tasks_controller.rb
module Api
  module V1
    class TasksController < ApplicationController
      before_action :set_task, only: [:show, :edit, :update, :destroy]
      protect_from_forgery :except => [:create, :update]

      # GET /tasks
      # GET /tasks.json
      def index
        # クエリパラメータによる指定があった場合
        unless params[:robot_id].blank? && params[:map_id].blank?
          # ロボットIDが指定されたタスクのみを返す場合
          unless params[:robot_id].blank?
            robot = Robot.find(params[:robot_id])
              unless robot.nil?
                @tasks = robot.tasks
              end
          else
            # マップIDが指定されたタスクのみを返す場合
            map = Map.find(params[:map_id])
            unless map.nil?
              @tasks = map.tasks
            end
          end
        else
          @tasks = Task.all
        end
      end

      # GET /tasks/1
      # GET /tasks/1.json
      def show
      end

      # GET /tasks/new
      def new
        @task = Task.new
      end

      # GET /tasks/1/edit
      def edit
      end

      # POST /tasks
      # POST /tasks.json
      def create
        @task = Task.new(task_params)

        respond_to do |format|
          if @task.save
            format.html { redirect_to @task, notice: 'Task was successfully created.' }
            format.json { render :show, status: :created, location: @task }
          else
            format.html { render :new }
            format.json { render json: @task.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /tasks/1
      # PATCH/PUT /tasks/1.json
      def update
        respond_to do |format|
          if @task.update(task_params)
            format.html { redirect_to @task, notice: 'Task was successfully updated.' }
            format.json { render :show, status: :ok, location: @task }
          else
            format.html { render :edit }
            format.json { render json: @task.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /tasks/1
      # DELETE /tasks/1.json
      def destroy
        @task.destroy
        respond_to do |format|
          format.html { redirect_to tasks_url, notice: 'Task was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_task
          @task = Task.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def task_params
          response = params.require(:task).permit(:task_type, :map_id, :map_name, :node_id, :node_name, :robot_id, :trial)
          # タスクタイプによる処理
          response = processing_task_type(response)
          # マップ名で登録
          response = trans_map_name2id(response)
          # ノード名で登録
          trans_node_name2id(response)
        end

        def trans_map_name2id(params)
          # マップ名で登録
          unless params[:map_name].nil?
            unless Map.find_by(name: params[:map_name]).nil?
              params[:map_id] = Map.find_by(name: params[:map_name]).id
            end
            params.delete(:map_name)
          end
          params
        end

        def trans_node_name2id(params)
          # ノード名で登録
          unless params[:node_name].nil?
            unless Node.find_by(name: params[:node_name]).nil?
              params[:node_id] = Node.find_by(name: params[:node_name]).id
            end
            params.delete(:node_name)
          end
          params
        end

        def processing_task_type(params)
          # task_type依存の処理
          # 定時連絡は解決すべきタスクに含めない
          if params[:task_type] == "Ordinary Contact"
            params[:is_resolved] = true
          end
          params
        end
    end
  end
end
