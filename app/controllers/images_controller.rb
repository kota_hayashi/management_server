class ImagesController < ApplicationController
  before_action :set_image, only: [:show, :edit, :update, :destroy]
  before_action :set_trial
  before_action :set_floor

  # GET /images
  # GET /images.json
  def index
    unless params[:trial].blank?
      @images = Image.where(trial: params[:trial]).page(params[:page]).per(25)
    else
      @images = Image.page(params[:page]).per(25)
    end
  end

  # GET /images/1
  # GET /images/1.json
  def show
  end

  # GET /images/new
  def new
    @image = Image.new
  end

  # GET /images/1/edit
  def edit
  end

  # POST /images
  # POST /images.json
  def create
    @image = Image.new(image_params)

    respond_to do |format|
      if @image.save
        format.html { redirect_to @image, notice: 'Image was successfully created.' }
        format.json { render :show, status: :created, location: @image }
      else
        format.html { render :new }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /images/1
  # PATCH/PUT /images/1.json
  def update
    image = @image.image
    image_params_tmp = image_params2
    image_params_tmp[:image] = image
    respond_to do |format|
      if @image.update(image_params_tmp)
        format.html { redirect_to @image, notice: 'Image was successfully updated.' }
        format.json { render :show, status: :ok, location: @image }
      else
        format.html { render :edit }
        format.json { render json: @image.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /images/1
  # DELETE /images/1.json
  def destroy
    @image.destroy
    respond_to do |format|
      format.html { redirect_to images_url, notice: 'Image was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # まとめて消す
  def destroy_multiple
    Image.destroy(params[:images])

    respond_to do |format|
      format.html { redirect_to images_path }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_image
      @image = Image.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def image_params
      params.require(:image).permit(:image, :map_id, :node_id, :image_cache, :remove_image, :begin_node, :comment, :trial)
    end

    def image_params2
      params.require(:image).permit(:map_id, :node_id, :begin_node, :comment, :trial)
    end

    def set_trial
      unless params[:trial].blank?
        @trial = params[:trial]
      else
        @trial = "all"
      end
    end

    def set_floor
      unless params[:floor].blank?
        @floor_id = params[:floor]
      else
        @floor_id = "1"
      end

      if Map.exists?(@floor_id)
        @map = Map.find(@floor_id)
      else
        @map = Map.first
        @floor_id = @map.id.to_s
      end
    end
end
