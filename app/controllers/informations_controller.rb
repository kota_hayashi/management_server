class InformationsController < ApplicationController
  before_action :set_trial
  before_action :set_floor
  before_action :select_images, only: [:map, :image]

  def root
  end

  def home
  end

  # GET /informations/complex
  def map
    unless params[:node].blank?
      node_id = params[:node]
    else
      node_id = @map.nodes.first.id
    end

    @node = Node.find(node_id)
    if @node.nil?
      @node = @map.nodes.first
    end

    @nodes = @map.nodes

    @images = @images_base.where(node_id: @node.id)
    @images_sub = []
    (@node.type_a_next_nodes + @node.type_b_next_nodes).each do |next_node|
    @images_sub.push(@images_base.where(node_id: next_node.id))
    end
  end

  def control
  end

  # GET /informations/image
  # ソートに使えるカラム(SQLインジェクション対応)
  VALID_SORT_COLUMNS = %w(begin_node node_id created_at)
  def image
    unless params[:sort].blank?
      sort = params[:sort] if VALID_SORT_COLUMNS.include?(params[:sort])
      @images_base = @images_base.order(sort)
      if params[:sort_option] == "down"
        @images_base = @images_base.reverse_order
      end
    end
    @images = @images_base.page(params[:page]).per(25)
  end

  def help
  end

  private
    def set_trial
      unless params[:trial].blank?
        @trial = params[:trial]
      else
        @trial = "all"
      end
    end

    def set_floor
      unless params[:floor].blank?
        @floor_id = params[:floor]
      else
        @floor_id = "1"
      end

      if Map.exists?(@floor_id)
        @map = Map.find(@floor_id)
      else
        @map = Map.first
        @floor_id = @map.id.to_s
      end
    end

    def select_images
      if @trial == "all"
        @images_base = Image
      else
        @images_base = Image.where(trial: @trial)
      end
    end
end
