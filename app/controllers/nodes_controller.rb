class NodesController < ApplicationController
  before_action :set_node, only: [:show, :edit, :update, :destroy]

  # GET /maps/1/nodes
  # GET /maps/1/nodes.json
  def index
    @map = Map.find(params[:map_id])
    @nodes = @map.nodes.all
  end

  # GET /maps/1/nodes/1
  # GET /maps/1/nodes/1.json
  def show
  end

  # GET /maps/1/nodes/new
  def new
    @node = Node.new
  end

  # GET /maps/1/nodes/1/edit
  def edit
  end

  # POST /maps/1/nodes
  # POST /maps/1/nodes.json
  def create
    @node = Node.new(node_params)

    respond_to do |format|
      if @node.save
        format.html { redirect_to @node, notice: 'Node was successfully created.' }
        format.json { render :show, status: :created, location: @node }
      else
        format.html { render :new }
        format.json { render json: @node.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /maps/1/nodes/1
  # PATCH/PUT /maps/1/nodes/1.json
  def update
    respond_to do |format|
      if @node.update(node_params)
        format.html { redirect_to @node, notice: 'Node was successfully updated.' }
        format.json { render :show, status: :ok, location: @node }
      else
        format.html { render :edit }
        format.json { render json: @node.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /maps/1/nodes/1
  # PATCH/PUT /maps/1/nodes/1.json
  def update_node
    respond_to do |format|
      if @node.update(node_params)
        format.html { redirect_to @node, notice: 'Map was successfully updated.' }
        format.json { render :show, status: :ok, location: @node }
      else
        format.html { render :edit }
        format.json { render json: @node.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /maps/1/nodes/1
  # DELETE /maps/1/nodes/1.json
  def destroy
    @node.destroy
    respond_to do |format|
      format.html { redirect_to nodes_url, notice: 'Node was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_node
      @map = Map.find(params[:map_id])
      @nodes = @map.nodes.all
      @node = @nodes.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def node_params
      params.require(:node).permit(:name, :node_file, :last_update, :node_id, :node_id)
    end
end
