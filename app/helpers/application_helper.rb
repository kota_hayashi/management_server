module ApplicationHelper
  # ページごとの完全なタイトルを返します。
  def full_title(page_title = '')
    base_title = "Management Server"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  # ページごとの完全なタイトルを返します。
  def navbar_title(page_title = '')
    base_title = "管理Server"
    if page_title.empty?
      base_title
    else
      base_title + " | " + page_title
    end
  end
end
