class Image < ApplicationRecord
  belongs_to :map
  belongs_to :node
  mount_base64_uploader :image, ImageUploader
  include CsvExportable
end
