class Link < ApplicationRecord
  belongs_to :node1, class_name:  'Node'
  belongs_to :node2, class_name:  'Node'
  #
  validates :node1_id, presence: true
  validates :node2_id, presence: true
end
