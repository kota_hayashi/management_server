class Map < ApplicationRecord
  has_many :nodes, dependent: :destroy
  has_many :robots
  has_many :tasks
end
