class Node < ApplicationRecord
  belongs_to :map

  has_many :node1_links, class_name: 'Link', :foreign_key => 'node1_id',
                                  dependent:   :destroy
  has_many :node2_links, class_name: 'Link', :foreign_key => 'node2_id',
                                  dependent:   :destroy

  has_many :type_a_next_nodes, through: :node1_links, source: :node2
  has_many :type_b_next_nodes, through: :node2_links, source: :node1

end
