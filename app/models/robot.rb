class Robot < ApplicationRecord
  belongs_to :map
  has_many :tasks
end
