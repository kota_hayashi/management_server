class Task < ApplicationRecord
  belongs_to :map
  belongs_to :node
  belongs_to :robot
end
