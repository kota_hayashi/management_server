json.extract! image, :id, :image, :map_id, :node_id, :created_at, :updated_at
json.url image_url(image, format: :json)
