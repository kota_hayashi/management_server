json.extract! map, :id, :name, :map_file, :last_update, :map_id, :node_id, :created_at, :updated_at
json.url map_url(map, format: :json)
