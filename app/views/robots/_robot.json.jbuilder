json.extract! robot, :id, :name, :map, :x, :y, :z, :last_conn, :ip_add, :port, :created_at, :updated_at
json.url robot_url(robot, format: :json)
