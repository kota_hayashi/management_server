json.extract! task, :id, :task_type, :map_id, :node_id, :robot_id, :created_at, :updated_at, :is_resolved
json.url task_url(task, format: :json)
