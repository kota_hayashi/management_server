Rails.application.routes.draw do

  root 'informations#root'

  # 情報提供用のルーティング
  get 'informations', to: 'informations#home'

  get 'informations/map', to: 'informations#map', as: 'map_mode'

  get 'informations/image', to: 'informations#image', as: 'image_mode'

  get 'informations/control', to: 'informations#control', as: 'control_mode'

  get 'informations/help', to: 'informations#help', as: 'help'

  # ブラウザ用のRobotsコントローラのルーティング
  resources :robots do
    collection do
      delete 'destroy_multiple'
    end
  end

  # ブラウザ用のMapsコントローラのルーティング
  resources :maps do
    collection do
      delete 'destroy_multiple'
    end
    resources :nodes
  end

  resources :images do
    collection do
      delete 'destroy_multiple'
    end
  end

  resources :tasks do
    collection do
      delete 'destroy_multiple'
    end
  end

  # apiコントローラのルーティング
  namespace :api, { format: 'json' } do
    namespace :v1 do
      resources :robots
      resources :maps do
        resources :nodes do
          collection do
            get ':id/nexts', to: 'nodes#show_next'
            get 'only_usable', to: 'nodes#show_usable'
          end
        end
        # collection do
        #   get '?map_name=', to: '#show_map_id'
        # end
      end
      resources :tasks
      resources :images

    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
