class CreateRobots < ActiveRecord::Migration[5.1]
  def change
    create_table :robots do |t|
      t.string :name
      t.string :map
      t.float :x
      t.float :y
      t.float :z
      t.datetime :last_conn
      t.string :ip_add
      t.integer :port

      t.timestamps
    end
  end
end
