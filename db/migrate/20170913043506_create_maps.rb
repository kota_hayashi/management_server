class CreateMaps < ActiveRecord::Migration[5.1]
  def change
    create_table :maps do |t|
      t.string :name
      t.string :map_file
      t.datetime :last_update
      t.references :map, foreign_key: true
      t.references :node, foreign_key: true

      t.timestamps
    end
  end
end
