class CreateNodes < ActiveRecord::Migration[5.1]
  def change
    create_table :nodes do |t|
      t.string :name
      t.float :x
      t.float :y
      t.float :z
      t.references :map, foreign_key: true
      t.references :node, foreign_key: true

      t.timestamps
    end
  end
end
