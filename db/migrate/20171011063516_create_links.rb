class CreateLinks < ActiveRecord::Migration[5.1]
  def change
    create_table :links do |t|
      t.integer :node1_id, index: true, foreign_key: true
      t.integer :node2_id, index: true, foreign_key: true

      t.timestamps
    end
  end
end
