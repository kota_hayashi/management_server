class AddUsableToNodes < ActiveRecord::Migration[5.1]
  def change
    add_column :nodes, :usable, :boolean, default: true
  end
end
