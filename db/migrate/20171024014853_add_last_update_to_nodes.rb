class AddLastUpdateToNodes < ActiveRecord::Migration[5.1]
  def change
    add_column :nodes, :last_update, :datetime, default: Time.zone.now
  end
end
