class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :images do |t|
      t.string :image
      t.references :map, foreign_key: true
      t.references :node, foreign_key: true

      t.timestamps
    end
  end
end
