class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.string :task_type
      t.references :map, foreign_key: true
      t.references :node, foreign_key: true
      t.references :robot, foreign_key: true

      t.timestamps
    end
  end
end
