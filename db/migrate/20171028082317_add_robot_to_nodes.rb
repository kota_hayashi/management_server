class AddRobotToNodes < ActiveRecord::Migration[5.1]
  def change
    add_reference :nodes, :robot, foreign_key: true
  end
end
