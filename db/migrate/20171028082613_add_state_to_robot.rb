class AddStateToRobot < ActiveRecord::Migration[5.1]
  def change
    add_column :robots, :state, :string
  end
end
