class RemoveMapFromRobots < ActiveRecord::Migration[5.1]
  def change
    remove_column :robots, :map, :string
  end
end
