class AddCommentToImages < ActiveRecord::Migration[5.1]
  def change
    add_column :images, :comment, :string
    add_column :images, :begin_node, :integer
  end
end
