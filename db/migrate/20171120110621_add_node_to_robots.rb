class AddNodeToRobots < ActiveRecord::Migration[5.1]
  def change
    add_column :robots, :node, :string
  end
end
