class AddTrialToImages < ActiveRecord::Migration[5.1]
  def change
    add_column :images, :trial, :integer
  end
end
