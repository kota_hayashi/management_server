class AddTrialToTasks < ActiveRecord::Migration[5.1]
  def change
    add_column :tasks, :trial, :integer
  end
end
