# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171205032613) do

  create_table "images", force: :cascade do |t|
    t.string "image"
    t.integer "map_id"
    t.integer "node_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "comment"
    t.integer "begin_node"
    t.integer "trial"
    t.index ["map_id"], name: "index_images_on_map_id"
    t.index ["node_id"], name: "index_images_on_node_id"
  end

  create_table "links", force: :cascade do |t|
    t.integer "node1_id"
    t.integer "node2_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["node1_id"], name: "index_links_on_node1_id"
    t.index ["node2_id"], name: "index_links_on_node2_id"
  end

  create_table "maps", force: :cascade do |t|
    t.string "name"
    t.string "map_file"
    t.datetime "last_update"
    t.integer "map_id"
    t.integer "node_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["map_id"], name: "index_maps_on_map_id"
    t.index ["node_id"], name: "index_maps_on_node_id"
  end

  create_table "nodes", force: :cascade do |t|
    t.string "name"
    t.float "x"
    t.float "y"
    t.float "z"
    t.integer "map_id"
    t.integer "node_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "usable", default: true
    t.datetime "last_update", default: "2017-10-24 01:49:45"
    t.integer "robot_id"
    t.index ["map_id"], name: "index_nodes_on_map_id"
    t.index ["node_id"], name: "index_nodes_on_node_id"
    t.index ["robot_id"], name: "index_nodes_on_robot_id"
  end

  create_table "robots", force: :cascade do |t|
    t.string "name"
    t.float "x"
    t.float "y"
    t.float "z"
    t.datetime "last_conn"
    t.string "ip_add"
    t.integer "port"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "state"
    t.integer "map_id"
    t.string "node"
    t.index ["map_id"], name: "index_robots_on_map_id"
  end

  create_table "tasks", force: :cascade do |t|
    t.string "task_type"
    t.integer "map_id"
    t.integer "node_id"
    t.integer "robot_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_resolved", default: false
    t.integer "trial"
    t.index ["map_id"], name: "index_tasks_on_map_id"
    t.index ["node_id"], name: "index_tasks_on_node_id"
    t.index ["robot_id"], name: "index_tasks_on_robot_id"
  end

end
