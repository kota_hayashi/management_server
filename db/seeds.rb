# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# ダミーデータ(ID:1をすベてダミーにする)
dummy_map = Map.create!(name: "dummy map",
map_file: "dummy",
last_update: 0,
)

Robot.create!(name: "dummy robot",
x: 0.0,
y: 0.0,
z: 0.0,
map: dummy_map,
last_conn: Time.zone.now,
ip_add: "dummy",
port: 0)

Node.create!(name: "dummy node",
x: 0.0,
y: 0.0,
z: 0.0,
map: dummy_map)


# 実データ
file_path = 'db/Nitech2-2V4-8FullGlobal.prd'
first = true
map = nil
make_time = Time.zone.now
nodes = {}
File.foreach(file_path) do |node|
  if node[0] != "#" and node != "
" then
    if first then
      first = false
      # マップの生成
      map = Map.create!(name: node,
      map_file: "~/???",
      last_update: make_time,
      )
      puts map
    else
      node_elements = node.split(",")
      # ノードの生成
      nodes[node_elements[0]] = Node.create!(name: node_elements[0],
      x: node_elements[1],
      y: node_elements[2],
      z: 0.0,
      map: map
      )

      # リンクの生成
      num = 0
      # 登録されている隣接ノード数分
      until num > node_elements[4].to_i do
        # 隣接ノードが登録済みなら
        unless nodes[node_elements[5+num]].nil? then
          Link.create!(node1: nodes[node_elements[0]],
            node2: nodes[node_elements[5+num]]
          )
        end
        num = num + 1
      end
    end
  end
end

# サンプルロボットデータ
Robot.create!(name: "robot1",
x: 1.0,
y: 1.0,
z: 1.0,
map: map,
last_conn: Time.zone.now,
ip_add: "localhost",
port: 3000)

Robot.create!(name: "robot2",
x: 1.0,
y: 1.0,
z: 1.0,
map: map,
last_conn: Time.zone.now,
ip_add: "localhost",
port: 3000)

Robot.create!(name: "robot3",
x: 1.0,
y: 1.0,
z: 1.0,
map: map,
last_conn: Time.zone.now,
ip_add: "localhost",
port: 3000)
