require 'test_helper'

class InformationsControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get informations_home_url
    assert_response :success
  end

  test "should get complex" do
    get informations_complex_url
    assert_response :success
  end

  test "should get map" do
    get informations_map_url
    assert_response :success
  end

  test "should get image" do
    get informations_image_url
    assert_response :success
  end

  test "should get help" do
    get informations_help_url
    assert_response :success
  end

end
